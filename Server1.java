// Nama : Ilham Pratama
// NRP  : 4210181020
// Server Receive File From Client


import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.Buffer;

public class Server1 {

    private static ServerSocket socket;
    static int port = 5656;

    private static void startTransfer(Socket ServerSock) throws IOException {   
        DataInputStream fileTransfer = new DataInputStream(ServerSock.getInputStream());
        FileOutputStream fileNameOutput = new FileOutputStream("a.exe");
        
		byte[] buffer = new byte[4096];
		
		int filesize = 1024 * 200000; // Send file size in separate msg
		int readSize = 0;
		int totalRead = 0;
		int remaining = filesize;
		while((readSize = fileTransfer.read(buffer, 0, Math.min(buffer.length, remaining))) > 0) {
			totalRead += readSize;
			remaining -= readSize;
			System.out.println("read " + totalRead + " bytes.");
			fileNameOutput.write(buffer, 0, readSize);
		}
		
		fileNameOutput.close();
		fileTransfer.close();
    }

    public static void main(String[] args) 
    {
        try {
            socket = new ServerSocket(port);
            while (true) {
                try {
                    Socket ServerSock = socket.accept();
                    startTransfer(ServerSock);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
}